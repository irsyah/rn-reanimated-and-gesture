import { View, Text, StyleSheet, Image } from 'react-native'
import React from 'react'
import { Gesture, GestureDetector, GestureHandlerRootView, PanGestureHandler, PinchGestureHandler } from 'react-native-gesture-handler'
import Animated, { useAnimatedGestureHandler, useAnimatedStyle, useSharedValue } from 'react-native-reanimated';

export default function GestureExample() {

  const scale = useSharedValue(1);
  const transX = useSharedValue(0);
  const transY = useSharedValue(0);

  const AnimatedImage = Animated.createAnimatedComponent(Image);

  const animatedStyle = useAnimatedStyle(() => ({
    transform: [
      { translateX: transX.value },
      { translateY: transY.value },
      { scale: scale.value }
    ]
  }))

  // GESTURE HANDLER
  const panHandler = useAnimatedGestureHandler({
    onActive: (event) => {
      transX.value = event.translationX,
      transY.value = event.translationY
    }
  })

  const pinchHandler = useAnimatedGestureHandler({
    onActive: (event) => {
      scale.value = event.scale
    }
  })

  return (
    <GestureHandlerRootView>
      <PinchGestureHandler onGestureEvent={pinchHandler}>
        <AnimatedImage style={[styles.image, animatedStyle]} source={{ uri: "https://wallpaperaccess.com/full/21964.jpg"}} />
        {/* <Animated.View style={[styles.box, animatedStyle ]}></Animated.View> */}
      </PinchGestureHandler>
    </GestureHandlerRootView>
  )

  // // GESTURE DETECTOR RACE
  // const pinch = Gesture.Pinch()
  // .onChange((event) => {
  //   scale.value = event.scale
  // })
  // .onEnd(() => {
  //   scale.value = 1
  // })

  // const pan = Gesture.Pan()
  // .onChange(({ translationX, translationY }) => {
  //   // console.log("masuk");
  //   console.log(translationX);
  //   transX.value = translationX,
  //   transY.value = translationY
  // })


  // const gesture = Gesture.Race(pinch, pan);

  // return (
  //   <GestureHandlerRootView>
  //     <GestureDetector gesture={gesture}>
  //       <Animated.View style={[styles.box, animatedStyle ]}></Animated.View>
  //     </GestureDetector>
      
  //   </GestureHandlerRootView>
  // )
}

const styles =  StyleSheet.create({
  image: {
    width: 200,
    height: 200
  },
  box: {
    width: 100,
    height: 100,
    backgroundColor: "salmon"
  }
})