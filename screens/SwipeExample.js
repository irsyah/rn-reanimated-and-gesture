import { View, Text, FlatList } from 'react-native'
import React, { useEffect, useState } from 'react'
import Todo from '../components/Todo';
import { GestureHandlerRootView } from 'react-native-gesture-handler';

export default function SwipeExample() {
  const [ todos, setTodos ] = useState([]);
  
  useEffect(() => {
    setTodos([
        { id: 1, title: 'Learn React Native' },
        { id: 2, title: 'Learn React Gesture Handler' },
        { id: 3, title: 'Learn React Animated' },
        { id: 4, title: 'Learn React Swipeable' }
    ])
  }, [])
  
  return (
    <GestureHandlerRootView>
      <FlatList 
        data={todos}
        renderItem={({ item }) => (
            <Todo todo={item} setTodos={setTodos} />
          )}
        keyExtractor={(item) => item.id}
      />
    </GestureHandlerRootView>
  )
}