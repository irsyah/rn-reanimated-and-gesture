import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { AntDesign } from '@expo/vector-icons'; 
import React from 'react';
import Animated, { BounceOutUp, Layout } from 'react-native-reanimated';

export default function Landscape({ item, setLandscapes}) {

    const removeLandscape = () => {
        setLandscapes(currState => {
            return currState.filter((state) => {
                if (state.id !== item.id) {
                    return state;
                }
            })
        })
    }

    return (
      
      <Animated.View style={styles.imageContainer} exiting={BounceOutUp} layout={Layout.duration(200)}>
        <Image resizeMode='stretch' style={styles.image} source={{ uri: item.image }}/>
        <Text>{item.place}</Text>
        <TouchableOpacity style={styles.delete} onPress={ removeLandscape }>
            <AntDesign name="delete" size={24} color="white" />
        </TouchableOpacity>
      </Animated.View>
    )
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 170,
        // borderRadius: 25
    },
    imageContainer: {
        paddingHorizontal: 20,
        marginVertical: 5,
        backgroundColor: '#cfe2f3',
        borderRadius: 25, 
    },
    delete: {
        position: 'absolute',
        alignSelf: 'flex-end',
        // marginLeft: 50
        marginVertical: 10,
        paddingHorizontal: 25
    }
});

