import { View, Text, StyleSheet, Alert, Touchable, TouchableOpacity } from 'react-native'
import React from 'react'
import { AntDesign } from '@expo/vector-icons'; 
import { Swipeable } from 'react-native-gesture-handler'

export default function Todo({ todo, setTodos }) {

  const swipeRightAction = () => {
    setTodos(currState => {
        return currState.filter(state => {
            if (state.id !== todo.id) {
                return state;
            }
        })
    })
  } 

  const RightSwipeAction = () => {
    return (
        // <TouchableOpacity onPress={swipeRightAction}>
        <View
          style={{
            backgroundColor: '#ff8303',
            justifyContent: 'center',
            alignItems: 'center',
            height: 70
          }}
        >
        
            <AntDesign name="delete" size={24} color="white" />
          {/* <Text
            style={{
              color: '#1b1a17',
              paddingHorizontal: 10,
              fontWeight: '600',
              paddingHorizontal: 30,
              paddingVertical: 20,
            }}
          >
            Delete
          </Text> */}
        </View>
        // </TouchableOpacity>
      );
    }

  return (
    <Swipeable 
        renderRightActions={RightSwipeAction}
        onSwipeableOpen={swipeRightAction}
    >
        <View style={styles.todoLabel}>
            <Text>{todo.title}</Text>
        </View>
    </Swipeable>
  )
}



const styles = StyleSheet.create({
    todoLabel: {
        width: "100%",
        height: 100,
        justifyContent: 'center'
    }
})