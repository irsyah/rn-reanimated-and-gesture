import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import AnimatedStyleUpdateExample from './screens/AnimatedStyleUpdateExample';
import GestureExample from './screens/GestureExample';
import Landscapes from './screens/Landscapes';
import SwipeExample from './screens/SwipeExample';

console.reportErrorsAsExceptions = false

export default function App() {
  return (
    <SafeAreaProvider style={styles.container}>
      <SafeAreaView>
        {/* <Landscapes /> */}
        {/* <GestureExample /> */}
        <SwipeExample />
      </SafeAreaView>
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // justifyContent: 'center',
    // alignItems: 'center'
  },
});
